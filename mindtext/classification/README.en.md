## Introduction

MindSpore text classification is an open source image classification toolbox based on MindSpore.

## License

This project is released under the [Apache 2.0 license](LICENSE).

## Benchmark and model zoo

Results and models are available in the [model zoo](docs/model_zoo.md).

Supported backbones:

- [x] FastText
- [x] TextCNN
- [x] TextRCNN
- [x] TextRNN_Att
- [x] Transformer


## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mind_spore/mindspore-segmentation/issues).

## Contributing

We appreciate all contributions to improve MindSpore Segmentation. Please refer to [CONTRIBUTING.md](./CONTRIBUTING.md) for the contributing guideline.

## Contributors

## Acknowledgement

MindSpore classification is an open source project that welcome any contribution and feedback.
We wish that the toolbox and benchmark could serve the growing research
community by providing a flexible as well as standardized toolkit to reimplement existing methods
and develop their own new classification methods.